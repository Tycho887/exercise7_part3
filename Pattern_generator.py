# we want to generate a mandelbrot set

import numpy as np

image_resulution = 64
iter = 32

version = '1.0.0'

def is_in_mandelbrot_set(c, max_iter): # c is a complex number
    z = 0
    for i in range(max_iter): # we want to iterate max_iter times
        z = z*z + c
        if abs(z) > 2:
            return i
    else: return max_iter


def mandelbrot_to_ascii(m, max_iter): # m is the number of iterations
    if m == max_iter:
        return ' '
    else:
        log_iter = int(np.ceil(np.log(m+1))) # we want to map the number of iterations to a character
        return chr(64 + log_iter)
    
def generate_ascii_art(min_x,min_y,max_x,max_y,iter=64):      # we want to generate a mandelbrot set
    for y in np.linspace(min_y,max_y,image_resulution):       # linspace' returns evenly spaced numbers over a specified interval
        for x in np.linspace(min_x,max_x,image_resulution*2): # we want to iterate over the x and y axis
            c = complex(x,y)                                  # we want to convert the x and y axis to a complex number
            m = is_in_mandelbrot_set(c,iter)                  # we want to check if the complex number is in the mandelbrot set
            print(mandelbrot_to_ascii(m,iter),end='')         # we want to print the ascii art
        print()

generate_ascii_art(-2,-1.5,1,1.5) # we want to generate a mandelbrot set

